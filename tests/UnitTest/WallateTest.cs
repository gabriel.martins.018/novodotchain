using System;
using DotChain;
using FluentAssertions;
using UnitTest.Shared;
using Xunit;
using Xunit.Abstractions;

namespace UnitTest;

public class WallateTest: Fixture
{
    public WallateTest(ITestOutputHelper outputHelper) : base(outputHelper)
    {
    }    

    [Fact]
    public void pubKey_shold_not_be_null()
    {
        var publicKey = Wallet.PublicKey;
        publicKey.Should().NotBeNullOrEmpty();
        
        OutputHelper.WriteLine(publicKey);
    }

    [Fact]
    public void public_address_shold_not_be_null()
    {
        var address = Wallet.PublicAddress;
        address.Should().NotBeNullOrEmpty();
        
        OutputHelper.WriteLine(address);
    }
    
    [Fact]
    public void sign_message()
    {
        const string message = "my sign message";
        var signature = Wallet.Sign(message);
        
        signature.Should().NotBeNullOrEmpty();
        OutputHelper.WriteLine(signature);
    }

    [Fact]
    public void message_verification_shold_be_true()
    {
        const string message = "my sign message";
        var signature = Wallet.Sign(message);

        Wallet.Verify(message, signature).Should().BeTrue();
    }
    
    [Fact]
    public void message_with_raw_pubk_verification_shold_be_true()
    {
        const string message = "my sign message";
        var pubkey = Wallet.PublicKey;
        var signature = Wallet.Sign(message);

        Wallet.Verify(message, signature, pubkey).Should().BeTrue();
    }    
    
    [Fact]
    public void message_verification_shold_be_false_for_fake_message()
    {
        const string realMessage = "my sign message";
        const string fakeMessage = "my super sign message";
        var signature = Wallet.Sign(realMessage);

        realMessage.Should().NotBeEquivalentTo(fakeMessage);
        Wallet.Verify(fakeMessage, signature).Should().BeFalse();
    }    
    
    [Fact]
    public void message_verification_with_pubkey_raw_shold_be_false_for_fake_message()
    {
        const string realMessage = "my sign message";
        const string fakeMessage = "my super sign message";
        var pubkey = Wallet.PublicKey;
        var signature = Wallet.Sign(realMessage);

        realMessage.Should().NotBeEquivalentTo(fakeMessage);
        Wallet.Verify(fakeMessage, signature, pubkey).Should().BeFalse();
    }      
    
    [Fact]
    public void message_verification_shold_be_false_for_fake_signature()
    {
        const string message = "my sign message";
        const string fakeSignature = "IN5v9+3HGW1q71OqQ1boSZTm0/DCiMpI8E4JB1nD67TCbIVMRk/e3KrTT9GvOuu3NGN0w8R2lWOV2cxnBp+Of8c=";
        Wallet.Verify(message, fakeSignature).Should().BeFalse();
    }  
    
    [Fact]
    public void message_verification_with_pubkey_raw_shold_be_false_for_fake_signature()
    {
        const string message = "my sign message";
        var pubkey = Wallet.PublicKey;
        const string fakeSignature = "IN5v9+3HGW1q71OqQ1boSZTm0/DCiMpI8E4JB1nD67TCbIVMRk/e3KrTT9GvOuu3NGN0w8R2lWOV2cxnBp+Of8c=";
        Wallet.Verify(message, fakeSignature, pubkey).Should().BeFalse();
    }
}