using System.ComponentModel.DataAnnotations;
using DotChain.Models;

namespace DotChain.Api.Models.Blockchain;

public record BlockStarInput
{
    [Required] public string Address { get; init; } = string.Empty;
    [Required] public string Message { get; init; } = string.Empty;
    [Required] public string Signature { get; init; } = string.Empty;
    [Required] public BlockStar Star { get; init; } = new ();
}