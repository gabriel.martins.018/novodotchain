namespace DotChain.Api.Models.Wallets;

public interface IWalletRepository
{
    void Add(Wallet wallet);
    Wallet? Get(string address);
}